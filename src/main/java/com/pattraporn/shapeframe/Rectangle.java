/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeframe;

/**
 *
 * @author Pattrapon N
 */
public class Rectangle extends Shape{

    private double h;
    private double l;

    public Rectangle(double h, double l) {
        super("Rectangle");
        this.h = h;
        this.l = l;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getL() {
        return l;
    }

    public void setL(double l) {
        this.l = l;
    }

    @Override
    public double calArea() {
        return h*l;
    }

    @Override
    public double calPerimeter() {
        return 2*(h+l);
    }
}
