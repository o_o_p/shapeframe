/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Pattrapon N
 */
public class RectangleFrame {
   public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(800 ,500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblh = new JLabel("h: ", JLabel.TRAILING);
        lblh.setSize(50, 20);
        lblh.setLocation(5, 5);
        lblh.setBackground(Color.WHITE);
        lblh.setOpaque(true);
        frame.add(lblh);
        
        JLabel lbll = new JLabel("l: ", JLabel.TRAILING);
        lbll.setSize(50, 20);
        lbll.setLocation(5, 25);
        lbll.setBackground(Color.RED);
        lbll.setOpaque(true);
        frame.add(lbll);

        final JTextField txth= new JTextField();
        txth.setSize(50, 20);
        txth.setLocation(60, 5);
        frame.add(txth);
        
        final JTextField txtl= new JTextField();
        txtl.setSize(50, 20);
        txtl.setLocation(60, 27);
        frame.add(txtl);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle h = ??? l = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 80);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.PINK);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strh = txth.getText();
                    String strl = txtl.getText();
                    double h = Double.parseDouble(strh);
                    double l = Double.parseDouble(strl);
                    Rectangle rectangle = new Rectangle(h,l);
                    lblResult.setText("Rectangle h ="
                            + String.format("%.2f", rectangle.getH())+ "Rectangle l ="+String.format("%.2f", rectangle.getL())  
                            + "area = " + String.format("%.2f", rectangle.calArea())
                            + "peimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input number", "Error "
                            , JOptionPane.ERROR_MESSAGE);
                    txth.setText("");
                    txth.requestFocus();
                    txtl.setText("");
                    txtl.requestFocus();
                }
            }

        });
        frame.setVisible(true);
    }
}
