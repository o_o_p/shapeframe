/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Pattrapon N
 */
public class RectangleFrame2 extends JFrame{
    JLabel lblh;
    JLabel lbll;
    JTextField txth;
    JTextField txtl;
    JButton btnCalculate;
    JLabel lblResult;
  public RectangleFrame2()  {
        super("Rectangle");
        this.setSize(800 ,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblh = new JLabel("h: ", JLabel.TRAILING);
        lblh.setSize(50, 20);
        lblh.setLocation(5, 5);
        lblh.setBackground(Color.WHITE);
        lblh.setOpaque(true);
        this.add(lblh);
        
        lbll = new JLabel("l: ", JLabel.TRAILING);
        lbll.setSize(50, 20);
        lbll.setLocation(5, 25);
        lbll.setBackground(Color.RED);
        lbll.setOpaque(true);
        this.add(lbll);

        txth= new JTextField();
        txth.setSize(50, 20);
        txth.setLocation(60, 5);
        this.add(txth);
        
        txtl= new JTextField();
        txtl.setSize(50, 20);
        txtl.setLocation(60, 27);
        this.add(txtl);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle h = ??? l = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 80);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.PINK);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strh = txth.getText();
                    String strl = txtl.getText();
                    double h = Double.parseDouble(strh);
                    double l = Double.parseDouble(strl);
                    Rectangle rectangle = new Rectangle(h,l);
                    lblResult.setText("Rectangle h ="
                            + String.format("%.2f", rectangle.getH())+ "Rectangle l ="+String.format("%.2f", rectangle.getL())  
                            + "area = " + String.format("%.2f", rectangle.calArea())
                            + "peimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame2.this, "Error : Please input number", "Error "
                            , JOptionPane.ERROR_MESSAGE);
                    txth.setText("");
                    txth.requestFocus();
                    txtl.setText("");
                    txtl.requestFocus();
                }
            }

        });}
         public static void main(String[] args) {
        RectangleFrame2 frame = new RectangleFrame2();
        frame.setVisible(true);

    }

}
