/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.shapeframe;

// Java program to demonstrate working
// of java.lang.Math.sqrt() method
import java.lang.Math;
/**
 *
 * @author Pattrapon N
 */
public class Triangle extends Shape{
    private double side;

    public Triangle(double side) {
        super("Triangle");
        this.side = side;
    }


    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    
    @Override
    public double calArea() {
        return Math.sqrt(3)/4*Math.pow(side, 2);
    }

    @Override
    public double calPerimeter() {
        return side*3;
    }
}

